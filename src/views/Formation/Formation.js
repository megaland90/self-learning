import React, {Component} from 'react';
import {
    Container,
    Row,
    Col,
    CardGroup,
    Card,
    CardBody,
    Button,
    Input,
    InputGroup,
    InputGroupAddon,
    CardHeader,
    ListGroup,
    ListGroupItem,
    ListGroupItemHeading,
    ListGroupItemText,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter, CardFooter, Badge
} from 'reactstrap';

import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import FilterSelector from "../../components/FilterSelector/FilterSelector";
import Comment from "../../components/Comment/Comment";
import FormationList from "./FormationList";



export default class Formation extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            formation : "Présentation"
        }

        this.formation =  [
            "Présentation",
            "Création du server",
            "Création du client",
            "Test unitaire",
            "Formation complémentaire"
            ];
    }

    render() {



        return (
            <div className="app">
                <Row>
                    <Col lg="9">
                        <Card hidden={false}>
                            <CardHeader>
                                {this.state.formation}
                                <div className="float-right">1 / 4</div>
                            </CardHeader>
                            <CardBody>
                                <iframe height="600px" width="100%" src="https://www.youtube.com/embed/0AqDNXDd_Mk"
                                        frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen></iframe>
                                <Card>
                                    <CardHeader>
                                        Information complémentaire
                                    </CardHeader>
                                    <CardBody>
                                        Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié.
                                    </CardBody>
                                </Card>
                                <Comment url="https://jsonplaceholder.typicode.com/posts/1" autoload={false}/>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="3">
                        <Card>
                            <CardHeader>
                                Chapitre
                            </CardHeader>
                            <FilterSelector defaultSelect="Présentation" onChange={(type)=>{this.setState({formation:type})}} types={this.formation}/>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}