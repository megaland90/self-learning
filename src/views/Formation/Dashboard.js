import React, {Component} from 'react';
import {
    Container,
    Row,
    Col,
    CardGroup,
    Card,
    CardBody,
    Button,
    Input,
    InputGroup,
    InputGroupAddon,
    CardHeader, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText
} from 'reactstrap';

import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import {BootstrapTable, TableHeaderColumn} from "react-bootstrap-table";
import FormationList from "./FormationList";
import Formation from "./Formation";


class Dashboard extends Component {

    constructor(props)
    {
        super(props);
    }



    render() {
    return (
      <div className="app">
          <FormationList/>
      </div>
    );
    }
}

export default Dashboard;
