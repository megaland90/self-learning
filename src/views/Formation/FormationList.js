import React, {Component} from 'react';
import {
    Container,
    Row,
    Col,
    CardGroup,
    Card,
    CardBody,
    Button,
    Input,
    InputGroup,
    InputGroupAddon,
    CardHeader,
    ListGroup,
    ListGroupItem,
    ListGroupItemHeading,
    ListGroupItemText,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
} from 'reactstrap';

import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import {BootstrapTable, TableHeaderColumn} from "react-bootstrap-table";
import FilterSelector from "../../components/FilterSelector/FilterSelector";


export default class FormationList extends Component
{
    constructor(props)
    {
        super(props);
        this.dataTable = [
            {
                'name': 'Créer un server en c',
                'description': "Creation d'un serveur en c a l'aide de scoket.",
                'category': 'Développement',
                'duration': '3h00',
                'action': 'Paglieta',
            },
            {
                'name': 'Créer un server windows',
                'category': 'Administration Système',
                'description' : 'Installation de windows server 2012 et administration',
                'duration': '13h00',
                'action': 'Paglieta',
            },
        ];

        this.state = {
            modalConfirmation : false,
            modalConfirmationData : {
                name : null
            },
            formationType : "En cours",
            formationDifficulty : null
        }

        this.formationTypes =  ["En cours",
                "All",
                "Administration Système",
                "Développement",
                "Outils",
                "Hardware"];
        this.formationDifficulties = [
            "Débutant",
            "Confirmé",
            "Expert",
        ]
        this.options = {
            sortIndicator: true,
            hideSizePerPage: true,
            paginationSize: 3,
            hidePageListOnlyOnePage: true,
            clearSearch: false,
            alwaysShowAllBtns: false,
            withFirstAndLast: false
        }

        this.requirementOption = {
            sortIndicator: true,
            hideSizePerPage: true,
            paginationSize: 3,
            hidePageListOnlyOnePage: true,
            clearSearch: false,
            alwaysShowAllBtns: false,
            withFirstAndLast: false
        }


        this.toggleModalConfirmation = this.toggleModalConfirmation.bind(this);
        this.formationAction = this.formationAction.bind(this);
        this.startFormation = this.startFormation.bind(this);
        this.continuFormation = this.continuFormation.bind(this);
        this.confirm = this.confirm.bind(this);
    }

    toggleModalConfirmation()
    {
        this.setState({
            modalConfirmation: !this.state.modalConfirmation
        });
    }

    confirm()
    {
        this.toggleModalConfirmation();
        window.location.hash="#/formation/dashboard";
    }

    startFormation(data)
    {
        this.setState({
            modalConfirmationData : data
        })
        this.toggleModalConfirmation()
    }
    continuFormation(data)
    {
        window.location.hash="#/formation/dashboard";
    }


    formationAction(cell, row)
    {
        return (
            <div>
                <Button color="dark" hidden={(this.state.formationType === "En cours")} onClick={()=>{this.startFormation(row)}}>Je commence !</Button>
                <Button color="dark" hidden={(this.state.formationType !== "En cours")} onClick={()=>{this.continuFormation(row)}}>Continuer</Button>
            </div>

        )
    }

    formationName(cell, row)
    {
        return(
            <div>
                <h3>{cell}</h3>
                <h6>{row.description}</h6>
            </div>
        );
    }


    render() {


        const selectRowProp = {
            mode: 'checkbox',
        };

        return (
            <div className="app">
                <Modal isOpen={this.state.modalConfirmation} toggle={this.toggleModalConfirmation} className="modal-lg">
                    <ModalHeader toggle={this.toggleModalConfirmation}>Prérequis | {this.state.modalConfirmationData.name}</ModalHeader>
                    <ModalBody>
                        <BootstrapTable selectRow={selectRowProp} data={this.dataTable} version="4" pagination options={this.options}>
                            <TableHeaderColumn isKey dataField="name" dataSort dataFormat={this.formationName}>Prérequis</TableHeaderColumn>
                            <TableHeaderColumn width="200px" dataField="category" dataSort>Catégorie</TableHeaderColumn>
                            <TableHeaderColumn width="100px" dataSort dataField="duration">Durée</TableHeaderColumn>
                        </BootstrapTable>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.confirm}>C'est parti !</Button>{' '}
                        <Button color="secondary" onClick={this.toggleModalConfirmation}>Annuler</Button>
                    </ModalFooter>
                </Modal>
                <Row>
                    <Col lg="10">
                        <div style={{backgroundColor:"#fff", padding:"20px"}}>
                            <BootstrapTable data={this.dataTable} version="4" search searchPlaceholder='Recherche' pagination options={this.options}>
                                <TableHeaderColumn isKey dataField="name" dataSort dataFormat={this.formationName}>Formation</TableHeaderColumn>
                                <TableHeaderColumn width="200px" dataField="category" dataSort>Catégorie</TableHeaderColumn>
                                <TableHeaderColumn width="100px" dataSort dataField="duration">Durée</TableHeaderColumn>
                                <TableHeaderColumn width="150px" dataField="action" dataFormat={this.formationAction}>{(this.state.formationType === "En cours") ? 'Je continue !' : 'Je commence !'}</TableHeaderColumn>
                            </BootstrapTable>
                        </div>

                    </Col>
                    <Col lg="2">
                        <FilterSelector defaultSelect="En cours" onChange={(type)=>{this.setState({formationType:type})}} style={{marginBottom:'50px'}} types={this.formationTypes}/>
                        <FilterSelector defaultSelect="Débutant" onChange={(type)=>{this.setState({formationDifficulty:type})}} style={{marginBottom:'50px'}} types={this.formationDifficulties}/>
                    </Col>
                </Row>
            </div>
        );
    }
}