import React, {Component} from 'react';
import { Nav, NavItem, Input, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge, Button, ButtonGroup } from 'reactstrap';
import FilterSelector from "../../../components/FilterSelector/FilterSelector";

class Inbox extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <div className="email-app mb-4">
          <FilterSelector style={{paddingLeft:"20px",paddingTop:"15px"}} types={["Boite de reception","Nouveau message", "Message envoyé", "Corbeille"]}/>
          <main className="inbox">
            <div className="toolbar">
              <ButtonGroup>
                <Button color="light"><span className="fa fa-envelope"></span></Button>
              </ButtonGroup>
              <ButtonGroup>
                <Button color="light"><span className="fa fa-mail-reply"></span></Button>
                <Button color="light"><span className="fa fa-mail-reply-all"></span></Button>
                <Button color="light"><span className="fa fa-mail-forward"></span></Button>
              </ButtonGroup>
              <Button color="light"><span className="fa fa-trash-o"></span></Button>
              <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle caret color="light">
                  <span className="fa fa-tags"></span>
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem>add label<Badge color="danger">Home</Badge></DropdownItem>
                  <DropdownItem>add label<Badge color="info">Job</Badge></DropdownItem>
                  <DropdownItem>add label<Badge color="success">Clients</Badge></DropdownItem>
                  <DropdownItem>add label<Badge color="warning">News</Badge></DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              <ButtonGroup className="float-right">
                <Button color="light"><span className="fa fa-chevron-left"></span></Button>
                <Button color="light"><span className="fa fa-chevron-right"></span></Button>
              </ButtonGroup>
            </div>

            <ul className="messages">
              <li className="message unread">
                  <div className="header">
                    <span className="from"><Input type="checkbox"/>Lukasz Holeczek</span>
                    <span className="date"><span className="fa fa-paper-clip"></span> 12/04/2012 15h20</span>
                  </div>
                  <div className="title">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  </div>
                  <div className="description">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                  </div>
              </li>
            </ul>
          </main>
        </div>
      </div>
    )
  }
}

export default Inbox;
