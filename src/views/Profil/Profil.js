import React, { Component } from 'react';
import {
    FormGroup,
    Button,
    Form,
    Label,
    Input,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    ListGroup,
    ListGroupItem,
    CardBody, InputGroup, Row, Col, Card
} from 'reactstrap';

class Profil extends Component {


    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            profil : {
                pseudo :"Megaland90",
                email : "megaland90@gmail.com",
                password : "sdsds",
                img : "https://png2.kisspng.com/20180320/rrw/kisspng-computer-icons-user-profile-google-account-photos-icon-account-5ab1462b691e17.6975516515215672754306.png"
            }
        };

        this.toggle = this.toggle.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handlePseudo= this.handlePseudo.bind(this);
        this.handlePassword= this.handlePassword.bind(this);
        this.save = this.save.bind(this);
        this.cancel = this.cancel.bind(this);
        this.loadProfil = this.loadProfil.bind(this)
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    save()
    {
        var toogle = this.toggle;
        this.toggle();
    }

    cancel()
    {
        this.loadProfil();
        this.toggle();
    }

    loadProfil()
    {

    }
    componentDidMount()
    {
        this.loadProfil();
    }

    handleEmail(e)
    {
        this.setState({
            profil: {...this.state.profil,email: e.target.value}
        })
    }

    handlePseudo(e)
    {
        this.setState({
            profil: {...this.state.profil,pseudo: e.target.value}
        })
    }

    handlePassword(e)
    {
        this.setState({
            profil: {...this.state.profil,password: e.target.value}
        })
    }

    render() {
        return (
            <div>
                <Label for="imgProfil">
                    <img  style={{marginBottom:"20px", marginTop:"20px" }} height="150px" width="150px" src={this.state.profil.img} />
                </Label>
                <Input hidden={true} id="imgProfil" type="file"/>
                <div>
                    <ListGroup className="col-lg-4">
                        <ListGroupItem><strong>Email </strong> {this.state.profil.email}</ListGroupItem>
                        <ListGroupItem><strong>Pseudo </strong> {this.state.profil.pseudo}</ListGroupItem>
                    </ListGroup>
                    <Button color="primary" className="px-4" onClick={this.toggle} style={{marginTop:'20px'}} >Modifier mes informations</Button>
                </div>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Modifier mes informations</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label for="email">Email</Label>
                                <Input type="email" name="email" value={this.state.profil.email} onChange={this.handleEmail} id="email" placeholder="Email" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="password">Mot de passe</Label>
                                <Input type="password" name="password" value={this.state.profil.password} onChange={this.handlePassword} id="password" placeholder="Mot de passe" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="confirmPassword">Confirmez mot de passe</Label>
                                <Input type="password" name="confirmPassword" value={this.state.profil.password} onChange={this.handlePassword} id="confirmPassword" placeholder="Mot de passe" />
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.save}>Enregistrer</Button>{' '}
                        <Button color="secondary" onClick={this.cancel}>Annuler</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}

export default Profil;
