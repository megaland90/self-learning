import React, {Component} from 'react';
import {Container, Row, Col, CardGroup, Card, CardBody, Button, Input, InputGroup, InputGroupAddon} from 'reactstrap';


class Login extends Component {

  constructor(props)
  {
      super(props)
      this.forgetPassword = this.forgetPassword.bind(this);
      this.connect = this.connect.bind(this);
  }

  connect()
  {
      window.location.hash ="#/profil";
  }

  forgetPassword()
  {
      window.location.hash ="#/forgetPassword";
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-4">
                <Card className="p-4">
                  <CardBody>
                    <h1>S'identifier</h1>
                    <p className="text-muted">Accéder à votre compte.</p>
                    <InputGroup className="mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="fa fa-envelope-o"></i>
                        </span>
                      </div>
                      <Input type="email" placeholder="Email"/>
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="icon-lock"></i>
                        </span>
                      </div>
                      <Input type="password" placeholder="Mot de passe"/>
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" className="px-4" onClick={this.connect}>Connexion</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Button color="link" className="px-0" onClick={this.forgetPassword}>Mot de passe oublié ?</Button>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary p-4" >
                  <CardBody>
                    <div>
                      <h2>Inscription</h2>
                      <p className="text-muted">Créer un compte.</p>
                      <InputGroup className="mb-3">
                          <div className="input-group-prepend">
                            <span className="input-group-text">
                              <i className="icon-user"></i>
                            </span>
                          </div>
                          <Input type="text" placeholder="Pseudo"/>
                      </InputGroup>
                      <InputGroup className="mb-3">
                          <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="fa fa-envelope-o"></i>
                      </span>
                          </div>
                          <Input type="email" placeholder="Email"/>
                      </InputGroup>
                      <InputGroup className="mb-4">
                          <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="icon-lock"></i>
                    </span>
                          </div>
                          <Input type="password" placeholder="Mot de passe"/>
                      </InputGroup>
                        <InputGroup className="mb-4">
                            <div className="input-group-prepend">
                              <span className="input-group-text">
                                <i className="icon-lock"></i>
                              </span>
                            </div>
                            <Input type="password" placeholder="Confirmez le mot de passe"/>
                        </InputGroup>
                        <Row>
                            <Col xs="6">
                                <Button color="primary" className="px-4" active>S'inscrire</Button>
                            </Col>
                        </Row>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
