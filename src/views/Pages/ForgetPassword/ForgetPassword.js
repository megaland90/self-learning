import React, {Component} from 'react';
import {Container, Row, Col, CardGroup, Card, CardBody, Button, Input, InputGroup, InputGroupAddon} from 'reactstrap';


class ForgetPassword extends Component {

  constructor(props)
  {
    super(props);
    this.cancel = this.cancel.bind(this);
  }

  cancel()
  {
    window.location.hash ="#/login"
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-4">
                <Card className="p-4">
                  <CardBody>
                    <h1>Mot de pass oublié</h1>
                    <p className="text-muted">Envoyer un mot de passe de récupération</p>
                    <InputGroup className="mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="fa fa-envelope-o"></i>
                        </span>
                      </div>
                      <Input type="text" placeholder="Email"/>
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" className="px-4">Envoyer</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                          <Button color="link" className="px-0" onClick={this.cancel}>Annuler</Button>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default ForgetPassword;
