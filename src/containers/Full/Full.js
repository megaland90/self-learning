import React, {Component} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Aside from '../../components/Aside/';
import Footer from '../../components/Footer/';

import Profil from '../../views/Profil/Profil'
import Achat from '../../views/Profil/Achat'
import Message from '../../views/Profil/Message'


import Planning from '../../views/Formation/Planning'
import FormationList from '../../views/Formation/FormationList'
import Formation from '../../views/Formation/Formation'
import ForgetPassword from "../../views/Pages/ForgetPassword";

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              <Switch>
                <Route path="/profil" name="Profil" component={Profil}/>
                <Route path="/mes-achats" name="Mes-achats" component={Achat}/>
                <Route path="/message" name="Message" component={Message}/>
                <Route path="/formation/planning" name="Dev" component={Planning}/>
                <Route path="/formation/dashboard" name="Dashboard" component={Formation}/>
                <Route path="/formation/list" name="Dashboard" component={FormationList}/>
                <Redirect from="/" to="/login"/>
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Full;
