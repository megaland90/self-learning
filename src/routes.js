const routes = {
  '/formation/dashboard': 'Dashboard',
  '/formation/list': 'Liste',
  '/formation/planning': 'Planning',
  '/formation': 'Formation',
  '/profil': 'Profil',
};
export default routes;
