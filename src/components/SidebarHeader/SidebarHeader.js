import React, {Component} from 'react';
import {Input} from "reactstrap";

class SidebarHeader extends Component {


  constructor(props)
  {
      super(props);

      this.state = {
        search : ""
      }

      this.handlerSearch = this.handlerSearch.bind(this);
  }

  handlerSearch(e)
  {
    this.setState({search : e.target.value});
  }

  render() {
     return (
       <div className="sidebar-header">
           <Input onChange={ e => this.handlerSearch(e)} placeholder="Recherche"/>
       </div>
    )
  }
}

export default SidebarHeader;
