import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Nav,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
  Button,
    Input
} from 'reactstrap';
import HeaderDropdown from './HeaderDropdown';
import Buttons from "../../views/Buttons/Buttons";

class Header extends Component {

  constructor(props) {
    super(props);
  }

  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }

  render() {
    return (
      <header className="app-header navbar">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler>
        <div className="navbar-brand"></div>
          <div style={{paddingLeft:"10px"}} className="d-md-down-none">
        </div>
        <Nav className="ml-auto" navbar>
            <Button color="primary">S'inscrire</Button>
            <Button color="primary" style={{marginLeft:"10px", marginRight:"10px"}}>Connexion</Button>
        </Nav>
      </header>
    );
  }
}

export default Header;
