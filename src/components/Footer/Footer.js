import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span><a href="/">self-learning.fr</a> &copy; 2019 Théo Mottet.</span>
      </footer>
    )
  }
}

export default Footer;
