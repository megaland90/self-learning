import React, {Component} from 'react';
import {
    Container,
    Row,
    Col,
    CardGroup,
    Card,
    CardBody,
    Button,
    Input,
    InputGroup,
    InputGroupAddon,
    CardHeader,
    ListGroup,
    ListGroupItem,
    ListGroupItemHeading,
    ListGroupItemText,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter, CardFooter, FormGroup, Form
} from 'reactstrap';

import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import BigCalendar from "react-big-calendar";
import FilterSelector from "../FilterSelector/FilterSelector";
import Store from "../Store/Store";


export default class Comment extends Store
{
    constructor(props)
    {
        super(props);
        this.state = {
            comment :"",
        }

        this.handleInputComment = this.handleInputComment.bind(this);
        this.push = this.push.bind(this);
    }

    handleInputComment(e)
    {
        this.setState({comment : e.target.value})
    }


    push()
    {
        this.store.add({pseudo : "Megaland90",
            text : this.state.comment,
            id : new Date().getTime()});
        this.setState({comment : ""});
    }

    componentDidMount()
    {
        super.componentDidMount();
        this.store.add({
            pseudo : "Megaland90",
            text: "Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié.\n",
            id : "0"
        });
    }

    render() {

        let comments = this.state.data.map((comment)=>{
            return (
                <Card key={comment.id}>
                    <CardBody>
                        <div>
                            <h5>{comment.pseudo}</h5>
                            {comment.text}
                        </div>
                        <div style={{marginTop:"30px"}}>
                            <Button color="primary"><i className="fa fa-thumbs-o-up"></i>  Utile</Button>
                            <Button color="danger" style={{marginLeft:"20px"}}>Signaler !</Button>
                            <Button color="danger" onClick={() => this.store.delete(comment)} style={{marginLeft:"20px"}}>Supprimer</Button>
                        </div>
                    </CardBody>
                </Card>
            );
        })
        return (
            <div>
                <Card>
                    <CardHeader>
                        Commentaire
                    </CardHeader>
                    <CardBody>
                        <div>
                            <FormGroup row style={{marginTop:"20px"}}>
                                <Col lg="12">
                                    <Input type="textarea" name="textarea-input" id="textarea-input" rows="5"
                                           placeholder="Votre commentaire..." value={this.state.comment} onChange={this.handleInputComment}/>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Button size="md" color="primary" onClick={this.push}>Envoyer</Button>
                            </FormGroup>
                        </div>

                        <div style={{marginTop:"50px"}}>
                            {comments}
                        </div>

                    </CardBody>
                </Card>


            </div>
        );
    }
}