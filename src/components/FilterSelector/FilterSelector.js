import React, {Component} from 'react';
import {
    ListGroup,
    ListGroupItem,
} from 'reactstrap';

import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

class FilterSelector extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            selector : this.props.defaultSelect
        }
    }

    selectorType(type, checkType = false)
    {

        if (checkType == false)
        {

            return (<ListGroupItem onClick={()=>{
                    this.setState(
                        {
                            selector : type
                        });
                    this.props.onChange(type);
                }} key={type} active={this.selectorType(type, true)} tag="button" action>{type}</ListGroupItem>
            );
        }
        else if(checkType == true)
        {
            return (this.state.selector === type);
        }
    }

    render() {

        let listGroupItem = this.props.types.map((type) => {
            return this.selectorType(type);
        })

        return (
            <ListGroup style={this.props.style}>
                {listGroupItem}
            </ListGroup>
        );
    }
}

FilterSelector.defaultProps  = {
    styles : {},
    types : [],
    defaultSelect : null,
    onChange : (value) => {

    }
}


export default FilterSelector