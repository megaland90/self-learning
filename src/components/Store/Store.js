import React, {Component} from 'react';

class Store extends Component
{
    constructor(props) {
        super(props);
        this.state = {
            data : this.props.data
        }
        this.store = {
            load : () => {
                fetch(this.props.url, this.props.request).then((response) => {
                    this.setState({
                        response: response,
                    });
                    response.json().then((data) => {
                        this.store.setState(data);
                    }).catch(function(err) {
                        console.error(err);
                    });
                }).catch(function(err) {
                    console.error(err);
                });
            },
            add : (item) => {
                let data = this.state.data;

                /*let request = {method:'put', body:item,...this.props.request}
                fetch(this.props.url, request).then((response) => {
                    this.setState({
                        response: response,
                    });
                    data = [item, ...data];
                    this.store.setState(data);
                }).catch(function(err) {
                    console.error(err);
                });*/
                data = [item, ...data];
                this.store.setState(data);

            },
            set : (item) => {
                let data = this.state.data;
                data = data.map(i => i === item ? item : i);
                this.store.setState(data);
            },
            delete : (item) => {
                let data = this.state.data;
                data = data.filter(i => i !== item);
                this.store.setState(data);
            },
            setState : (data) => {
                this.setState({data:data});
                this.props.onStoreChange(data);
            }
        }
    }

    componentWillMount()
    {
        this.setState({
            data: [],
            response : {}
        });
    }

    componentDidMount()
    {
        if(this.props.autoload)
        {
            this.store.load();
        }
    }
}


Store.defaultProps = {
    request : {},
    url : "",
    dtat : [],
    autoload : true,
    onStoreChange : (data) => {}
}


export default Store;