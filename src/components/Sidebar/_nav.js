export default {
  items: [
      {
          title: true,
          name: 'Profil',
          wrapper: {            // optional wrapper object
              element: 'span',      // required valid HTML5 element tag
              attributes: {}        // optional valid JS object with JS API naming ex: { className: 'my-class', style: { fontFamily: 'Verdana' }, id: 'my-id'}
          },
          class: 'text-center'             // optional class names space delimited list for title item ex: 'text-center'
      },
      {
          name: 'Mon Profil',
          url: '/profil',
          icon: 'icon-puzzle'
      },
      {
          name: 'Message',
          url: '/message',
          icon: 'icon-puzzle'
      },
      {
          name: 'Mes achats',
          url: '/mes-achats',
          icon: 'icon-puzzle'
      },
    {
      title: true,
      name: 'Formation',
      wrapper: {            // optional wrapper object
        element: 'span',      // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: 'my-class', style: { fontFamily: 'Verdana' }, id: 'my-id'}
      },
      class: 'text-center'             // optional class names space delimited list for title item ex: 'text-center'
    },
    {
        name: 'Dashboard',
        url: '/formation/list',
        icon: 'icon-puzzle'
    },
    {
      name: 'Planning',
      url: '/formation/planning',
      icon: 'icon-calendar'
    },
    {
      title: true,
      name: 'Live',
      wrapper: {            // optional wrapper object
          element: 'span',      // required valid HTML5 element tag
          attributes: {}        // optional valid JS object with JS API naming ex: { className: 'my-class', style: { fontFamily: 'Verdana' }, id: 'my-id'}
      },
      class: 'text-center'             // optional class names space delimited list for title item ex: 'text-center'
    },
    {
      name: 'Live',
      url: '/formation/dev',
      icon: 'icon-puzzle'
    },
    {
      name: 'Planning',
      url: '/formation/dev',
      icon: 'icon-calendar'
    },
  ]
};
